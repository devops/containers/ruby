# v4

- Test run fixed by updating rails-test-app image reference.

# v3

- shared-mime-info package added.

# v2

- `USER` is set in Dockerfile to non-root app-user
  and `--assemble-user=0` option is required in `s2i build`.

# v1

First versioned branch
