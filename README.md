# Ruby application builder images

These images are based on the Docker official Ruby images.
`GEM_HOME` is set to `/usr/local/bundle` and Bundler is
configured to install gems to that location.

Supported Ruby major versions: 2.6, 2.7, and 3.0.  Minor versions vary.

Rails and Bundler versions have been pinned to the Ruby major version:

Ruby 2.6:
  - Rails 5.2
  - Bundler 2.0.2

Ruby 2.7:
  - Rails 6.0
  - Bundler 2.1.4

Ruby 3.0:
  - Bundler 2.x (not pinned)
  - Rails 6.1

Also installed:
  - Node.js (version 12.x - others are possible)
  - Yarn (latest version installed globally via NPM)
  - PostgreSQL client

## Build

    $ make [ruby_major=[2.6|2.7|3.0]]

## Test

	$ make clean test [ruby_major=[2.6|2.7|3.0]]

## Source-to-image

    $ s2i build [SRC] gitlab-registry.oit.duke.edu/devops/containers/ruby:[RUBY_MAJOR]-[IMAGE_VERSION] --assemble-user=0 [OPTIONS]

Example (bash shell):

    $ s2i build "file://$(pwd)" gitlab-registry.oit.duke.edu/devops/containers/ruby:2.6-main --assemble-user=0 --incremental --copy

Pull and run the image for build usage, for example:

	$ docker pull gitlab-registry.oit.duke.edu/devops/containers/ruby:2.6-main
    $ docker run --rm gitlab-registry.oit.duke.edu/devops/containers/ruby:2.6-main

## Runtime Options

By default the assembled runtime image runs `puma`.
