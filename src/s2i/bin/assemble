#!/bin/bash
#
# Assemble application runtime image
#
if [[ "$(id -u)" -ne 0 ]]; then
    echo "ERROR: Assemble script requires root privileges." 1>&2
    echo "       Use s2i build option '--assemble-user=0'." 1>&2
    exit 1
fi

set -e

export -p

source $S2I_BIN/assemble-defaults

#
# Install Node.js and extra dependencies
#
# https://github.com/nodesource/distributions#installation-instructions
#
echo "---> Installing Node.js ..."
curl -sL https://deb.nodesource.com/setup_${NODEJS_MAJOR}.x | bash -
apt-get -y update
apt-get -y install nodejs

if [[ -n $EXTRA_DEPENDENCIES ]]; then
    echo "---> Installing extra dependencies ..."
    apt-get -y install $EXTRA_DEPENDENCIES
fi

if [[ -n $INSTALL_SCRIPTS ]]; then
    echo "---> Running extra install scripts ..."

    for s in ${INSTALL_SCRIPTS[@]}; do
	script="${S2I_BIN}/install-scripts/${s}"

	if [[ ! -f $script ]]; then
	    echo "ERROR: Install script not found: ${script}" 1>&2
	    exit 1
	fi

	$script
    done
fi

echo "---> Cleaning up ..."
apt-get -y clean
rm -rf /var/lib/apt/lists/*

# Inject source files
$S2I_BIN/inject-source-files $SOURCE_FILES $APP_ROOT

# Restore artifacts, if available
$S2I_BIN/restore-artifacts $SAVED_ARTIFACTS $GEM_HOME

#
# DON'T EXECUTE RAKE TASKS BEFORE THIS!
#
if [[ $YARN_INSTALL_GLOBALLY == 'true' ]]; then
    npm install -g yarn
fi

# Install specific Bundler version
gem install "bundler:${BUNDLER_VERSION}"

echo "---> Installing bundle ..."
bundle install --jobs="$(nproc)"
#
# OK, NOW YOU CAN EXECUTE RAKE TASKS. :)
#

# Ensure Rails tmp dirs exist
bundle exec rake tmp:create

# Clobber database config
if [[ $CLOBBER_DB_CONFIG == 'true' ]]; then
    cat > ./config/database.yml <<EOF
test:
  adapter: postgresql
development:
  adapter: postgresql
production:
  adapter: postgresql
EOF
fi

$S2I_BIN/precompile-assets

# Mark cache directories for save-artifiacts
$S2I_BIN/mark-cache-dirs $CACHE_DIRS

$S2I_BIN/fix-permissions
