#!/bin/bash
#
# Usage information on this s2i builder image.
#
source $S2I_BIN/assemble-defaults

cat <<EOF

SUMMARY

   s2i build SOURCE_REPO THIS_IMAGE RUNTIME_IMAGE_TAG [OPTIONS]

ENVIRONMENT VARIABLES

These variables can be passed to the build using the -e VAR=VALUE or -E ENVFILE
command line options, or by setting values in .s2i/environment in the project
source repository.

BUNDLER_VERSION       [default: $BUNDLER_VERSION]
  Version of bundler to install.
  Default version is bound to the Ruby major version which
  is fixed in the builder image.

CLOBBER_DB_CONFIG     [default: $CLOBBER_DB_CONFIG]
  Flag to overwrite config/database.yml from source with
  boilerplate config - set to 'true' to enable, other value
  to disable.

EXTRA_DEPENDENCIES    [default: undefined]
  Space-separated list of extra packages to install.

INSTALL_SCRIPTS       [default: undefined]
  A space-separated list of extra scripts to run at assemble time.

Available scripts:

$(ls -1 $S2I_BIN/install-scripts)

SAVED_ARTIFACTS       [default: $SAVED_ARTIFACTS]
  Location of injected artifiacts, if any.

SOURCE_FILES          [default: $SOURCE_FILES]
  Location of injected source files.

NODEJS_MAJOR          [default: $NODEJS_MAJOR]
  Major version of Node.js to install. Must be available from
  https://github.com/nodesource/distributions.

YARN_INSTALL_GLOBALLY [default: $YARN_INSTALL_GLOBALLY]
  Flag to install yarn globally - set to 'true' to enable,
  other value to disable.

EOF
